export default class Utils {
    static getRandomId(): string {
        return 'id-' + Math.floor(12345678 * Math.random()) + '-' + Math.floor(12345678 * Math.random());
    }
}
