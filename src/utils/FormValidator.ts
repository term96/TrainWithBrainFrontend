export default class FormValidator {
    private static readonly emailRegExp = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);
    private static readonly usernameRegExp = new RegExp(/^[a-z][a-z,0-9]*$/i);

    static isEmailValid(email: string): boolean {
        return !!email && email.length < 100 && FormValidator.emailRegExp.test(email);
    }

    static isPasswordValid(password: string): boolean {
        return !!password && password.length >= 8
            && password.search(/[a-z]/i) >= 0
            && password.search(/[0-9]/) >= 0;
    }

    static isUsernameValid(username: string): boolean {
        return !!username && username.length < 50 && FormValidator.usernameRegExp.test(username);
    }
}
