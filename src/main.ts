import './hooks';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Messages from './locale/Messages';

Vue.config.productionTip = false;

Vue.filter('message', (value: string) => {
    return Messages[value] || '[MISSING]';
});

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
