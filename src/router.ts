import Vue from 'vue';
import Router from 'vue-router';
import HomePage from './views/HomePage.vue';
import AuthPage from './views/AuthPage.vue';
import ProfilePage from './views/ProfilePage.vue';
import NewTrainingPage from './views/NewTrainingPage.vue';
import TrainingPage from './views/TrainingPage.vue';
import MyTrainingsPage from './views/MyTrainingsPage.vue';
import TrainingDiaryPage from '@/views/TrainingDiaryPage.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [

        {
            path: '/auth/:mode',
            name: 'auth',
            component: AuthPage,
            props: true
        },
        {
            path: '/profile',
            name: 'profile',
            component: ProfilePage
        },
        {
            path: '/newTraining',
            name: 'newTraining',
            component: NewTrainingPage
        },
        {
            path: '/training/:trainingId',
            name: 'training',
            component: TrainingPage,
            props: true
        },
        {
            path: '/myTrainings',
            name: 'myTrainings',
            component: MyTrainingsPage
        },
        {
            path: '/diary',
            name: 'diary',
            component: TrainingDiaryPage
        },
        {
            path: '/*',
            name: 'home',
            component: HomePage,
        }
    ],
});

export default router;
