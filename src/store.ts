import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';
import {SignInRequest, SignUpRequest} from '@/models/api/Auth';
import {ProfileInfoRequest, ProfileInfoResponse} from '@/models/api/Profile';
import {AllTrainingsRequest, MyTrainingsRequest, NewTrainingRequest, TakeTrainingRequest, ThrowTrainingRequest, TrainingInfoRequest} from '@/models/api/Trainings';
import {NewDiaryNoteRequest, TrainingDiaryRequest} from '@/models/api/TrainingDiary';
import {TrainingPreview} from '@/models/Training';
import {TrainingDiaryNote} from '@/models/TrainingDiary';

Vue.use(Vuex);

const API_BASE_URL = 'http://192.168.1.105:9000';
// const API_BASE_URL = 'http://asd';
const loadTokenFromStorage = (): string => {
    let token: string = '';
    try {
        token = localStorage.getItem('token') || '';
    } catch (_) {
    }

    return token;
};

export interface ResponseInfo<T> {
    requestId: string;
    isSuccess: boolean;
    response: T;
}

export default new Vuex.Store({
    state: {
        token: loadTokenFromStorage(),
        username: '',
        email: '',
        responses: <{ [index: string]: ResponseInfo<any> }> {},
        myTrainings: <TrainingPreview[]> [],
        diaryNotes: <TrainingDiaryNote[]> []
    },

    getters: {
        getResponseInfo: (state) => (requestId: number) => {
            return state.responses[requestId];
        }
    },

    mutations: {
        setToken: (state, token: string) => {
            state.token = token;
            try {
                localStorage.setItem('token', token);
            } catch (_) {
            }
        },

        logout: (state) => {
            state.token = '';
            state.username = '';
            state.email = '';

            try {
                localStorage.removeItem('token');
            } catch (_) {
            }
        },

        setProfileInfo: (state, profileInfo: ProfileInfoResponse) => {
            state.username = profileInfo.username;
            state.email = profileInfo.email;
        },

        saveResponseInfo: (state, responseInfo: ResponseInfo<any>) => {
            state.responses[responseInfo.requestId] = responseInfo;
        },

        setMyTrainings: (state, trainings: TrainingPreview[]) => {
            state.myTrainings.splice(0, state.myTrainings.length, ...trainings);
        },

        takeTraining: (state, training: TrainingPreview) => {
            state.myTrainings.push(training);
        },

        throwTraining: (state, training: TrainingPreview) => {
            for (let i = 0; i < state.myTrainings.length; i++) {
                if (state.myTrainings[i].id == training.id) {
                    return state.myTrainings.splice(i, 1);
                }
            }
        },

        setDiaryNotes: (state, notes: TrainingDiaryNote[]) => {
            state.diaryNotes.splice(0, state.diaryNotes.length, ...notes);
        },

        addDiaryNote: (state, note: TrainingDiaryNote) => {
            state.diaryNotes.push(note);
        }
    },

    actions: {
        signIn: (context, payload: SignInRequest) => {
            return Axios.post(API_BASE_URL + '/user/signin', payload).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        signUp: (context, payload: SignUpRequest) => {
            return Axios.post(API_BASE_URL + '/user/signup', payload).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        loadProfileInfo: (context, payload: ProfileInfoRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.get(API_BASE_URL + '/user', config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        loadAllTrainings: (context, payload: AllTrainingsRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.get(API_BASE_URL + '/trainings', config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        loadMyTrainings: (context, payload: MyTrainingsRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.get(API_BASE_URL + '/trainings/my', config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        createTraining: (context, payload: NewTrainingRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.post(API_BASE_URL + '/trainings', payload, config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        loadTrainingInfo: (context, payload: TrainingInfoRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                },
                params: payload
            };

            return Axios.get(API_BASE_URL + '/trainings/train', config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        takeTraining: (context, payload: TakeTrainingRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.post(API_BASE_URL + '/trainings/my/add', payload, config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        throwTraining: (context, payload: ThrowTrainingRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.post(API_BASE_URL + '/trainings/my/remove', payload, config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        loadDiaryInfo: (context, payload: TrainingDiaryRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.get(API_BASE_URL + '/diary', config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        },

        addDiaryNote: (context, payload: NewDiaryNoteRequest) => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            };

            return Axios.post(API_BASE_URL + '/diary', payload, config).then((response) => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: true, response: response.data});
            }).catch(() => {
                context.commit('saveResponseInfo', {requestId: payload.requestId, isSuccess: false});
            });
        }
    },
});
