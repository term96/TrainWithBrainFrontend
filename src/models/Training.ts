export interface TrainingPreview {
    id: string;
    name: string;
    description: string;
    imageUrl: string;
}

export interface Training {
    id: string;
    name: string;
    description: string;
    isTaken: boolean;
    levels: TrainingLevel[];
    imageUrls: string[];
}

export interface TrainingLevel {
    exercises: Exercise[];
}

export interface Exercise {
    name: string;
    description: string;
    imageUrl: string;
}
