export interface TrainingDiaryNote {
    id: string;
    trainingId: string;
    date: number;
    comment: string;
}

export interface DiaryTrainingName {
    id: string;
    name: string;
}
