export interface SignInRequest {
    requestId: string;
    username: string;
    password: string;
}

export interface SignUpRequest {
    requestId: string;
    username: string;
    password: string;
    email: string;
}

export interface SignInResponse {
    token: string;
    errors: {
        notFound: boolean;
    }
}

export interface SignUpResponse {
    token: string;
    errors: {
        usernameOccupied: boolean;
        emailOccupied: boolean;
    }
}
