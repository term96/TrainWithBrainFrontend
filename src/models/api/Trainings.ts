import {Training, TrainingPreview} from '@/models/Training';

export interface AllTrainingsRequest {
    requestId: string;
}

export interface AllTrainingsResponse {
    trainings: TrainingPreview[];
}

export interface MyTrainingsRequest {
    requestId: string;
}

export interface MyTrainingsResponse {
    isBadToken: boolean;
    trainings: TrainingPreview[];
}

export interface NewTrainingRequest {
    requestId: string;
    training: Training;
}

export interface NewTrainingResponse {
    isBadToken: boolean;
    trainingId: string;
}

export interface TrainingInfoRequest {
    requestId: string;
    trainingId: string;
}

export interface TrainingInfoResponse {
    isBadToken: boolean;
    training: Training;
}

export interface TakeTrainingRequest {
    requestId: string;
    trainingId: string;
}

export interface TakeTrainingResponse {
    isBadToken: boolean;
    isSuccess: boolean;
}

export interface ThrowTrainingRequest {
    requestId: string;
    trainingId: string;
}

export interface ThrowTrainingResponse {
    isBadToken: boolean;
    isSuccess: boolean;
}
