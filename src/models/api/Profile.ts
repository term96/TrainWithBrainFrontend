export interface ProfileInfoRequest {
    requestId: string;
}

export interface ProfileInfoResponse {
    isBadToken: boolean;
    username: string;
    email: string;
}
