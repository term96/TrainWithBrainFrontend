import {TrainingDiaryNote} from '@/models/TrainingDiary';

export interface TrainingDiaryRequest {
    requestId: string;
}

export interface TrainingDiaryResponse {
    isBadToken: boolean;
    notes: TrainingDiaryNote[];
}

export interface NewDiaryNoteRequest {
    requestId: string;
    note: TrainingDiaryNote;
}

export interface NewDiaryNoteResponse {
    isBadToken: boolean;
    isSuccess: boolean;
}
