interface Messages {
    [index: string]: string;
}

export default <Messages> {
    'action.signUp': 'Зарегистрироваться',
    'action.signIn': 'Войти в систему',
    'action.retry': 'Повторить',

    'auth.isRegistered': 'Уже зарегистрированы?',
    'auth.isNotRegistered': 'Ещё не зарегистрированы?',

    'label.username': 'Имя пользователя',
    'label.email': 'Email',
    'label.password': 'Пароль',
    'label.passConfirm': 'Повторите пароль',
    'label.selectTraining': 'Выберите тренировку',
    'label.noteComment': 'Комментарий',

    'placeholder.username': 'Например, maxpower17',
    'placeholder.email': 'Например, name@domain.com',
    'placeholder.password': 'Например, XxGOMR2MXQ',
    'placeholder.passConfirm': 'То же, что и в поле "пароль"',
    'placeholder.noteComment': 'Укажите здесь количество подходов и повторов, Ваше самочувствие и другую важную информацию',

    'validation.emailRequired': 'Введите email',
    'validation.emailInvalid': 'Неправильный email',
    'validation.emailOccupied': 'Такой email уже зарегистрирован',
    'validation.passwordRequired': 'Введите пароль',
    'validation.passwordInvalid': 'Пароль не подходит (не менее 8 символов, буквы и цифры)',
    'validation.passConfirmRequired': 'Повторите пароль для подтверждения',
    'validation.passConfirmInvalid': 'Не совпадает с паролем',
    'validation.usernameRequired': 'Введите имя пользователя',
    'validation.usernameInvalid': 'Имя пользователя не подходит (буквы и цифры, начинается с буквы)',
    'validation.usernameOccupied': 'Имя пользователя занято, попробуйте другое',
    'validation.required': 'Заполните это поле',

    'page.signIn': 'Вход',
    'page.signUp': 'Регистрация',
    'page.profile': 'Мой профиль',
    'page.trainings': 'Все программы',
    'page.myTrainings': 'Мои программы',
    'page.newTraining': 'Новая программа',
    'page.logout': 'Выйти',
    'page.training': 'Информация о программе',
    'page.diary': 'Дневник тренировок',

    'error.send': 'Не удалось отправить данные. Проверьте подключение к сети и попробуйте снова',
    'error.load': 'Не удалось загрузить данные. Проверьте подключение к сети и попробуйте снова',
    'error.userNotFound': 'Пользователь не найден, проверьте имя пользователя и пароль',

    'trainingPreview.open': 'Перейти к полному описанию',
    'trainingPreview.throw': 'Удалить из списка моих программ',

    'trainingPreviewList.empty': 'Список программ пуст',

    'training.isTaken': 'Вы занимаетесь по этой программе',
    'training.takeTraining': 'Добавить в список моих программ',
    'training.auth': 'Войдите в систему для тренировки по этой программе',

    'diaryList.empty': 'У Вас пока нет записей',
    'diaryList.addNote': 'Добавить запись',
    'diaryList.newNote': 'Новая запись',
    'diaryList.removed': 'Тренировка удалена',
    'diaryList.close': 'Закрыть',
    'diaryList.noTrainings': 'Начните тренироваться по одной из программ, чтобы добавить новую запись',

    'editTraining.trainingName': 'Название программы',
    'editTraining.trainingDescription': 'Описание',
    'editTraining.exerciseName': 'Название упражнения',
    'editTraining.exerciseDescription': 'Описание',
    'editTraining.addLevel': 'Добавить уровень',
    'editTraining.addExercise': 'Добавить упражнение',
    'editTraining.info': 'Информация',
    'editTraining.images': 'Изображения',
    'editTraining.remove': 'Удалить',
    'editTraining.save': 'Сохранить',

    'imagesCarousel.alt': 'Изображение программы тренировок',

    'common.level': 'Уровень'
}
